import argparse
import numpy as np
import pandas as pd
import os, pydicom
import cv2
from sklearn.model_selection import train_test_split
from collections import Counter
from PIL import Image
from tqdm import tqdm

def save_img_from_dcm(dcm_dir, img_dir, patient_id, patient_class):
    
    """ 
    Description - Function that loads a directory of dicom files, converts them to png and saves them.
        
    :type dcm_dir: str
    :param dcm_dir: Path to dicom image directory
    
    :type img_dir: str
    :param img_dir: Path to store png images
    
    :type patient_id: str
    :param patient_id: Unique ID of patient
    
    :type patient_class: str
    :param patient_class: 0 for negetive class and 1 for positive class
    """
    img_fp = os.path.join(img_dir,patient_class, "{}.png".format(patient_id))
    if os.path.exists(img_fp):
        return
    dcm_fp = os.path.join(dcm_dir, "{}.dcm".format(patient_id))
    img_1ch = pydicom.read_file(dcm_fp).pixel_array
    img_3ch = np.stack([img_1ch]*3, -1)
    img_3ch = cv2.resize(img_3ch, (224,224))
    im = Image.fromarray(img_3ch)
    img_fp = os.path.join(img_dir,patient_class, "{}.png".format(patient_id))
    
    im.save(img_fp)

def preprocess(args):
    """ 
    Description - Preprocessing function for the dataset
    :type args: list
    :param args: List of arguments to parse
    """
    csv_data = pd.read_csv(args.CSV)
    target = csv_data[['patientId','Target']]
    target = target.drop_duplicates(['patientId'])
    target['Target'] = target['Target'].astype('str')

    train_df, test_df = train_test_split(target, test_size=0.2, random_state=42)
    train_df, val_df = train_test_split(train_df, test_size=0.2, random_state=42)

    print("Train shape" ,train_df.shape, Counter(train_df['Target']), sep="   ")
    print("Val shape" ,val_df.shape, Counter(val_df['Target']), sep="   ")
    print("Test shape" ,test_df.shape, Counter(test_df['Target']), sep="   ")

    dcm_dir = args.dcmdir
    img_dir = args.traindir
    for row in tqdm(range(  len(train_df))):
        patientId , pclass = train_df.iloc[row]
        save_img_from_dcm(dcm_dir, img_dir, patientId, pclass)
    print("Training data saved as png")

    img_dir = args.valdir
    for row in tqdm(range(len(val_df))):
        patientId , pclass = val_df.iloc[row]
        save_img_from_dcm(dcm_dir, img_dir, patientId, pclass)
    print("Val saved as png")

    img_dir = args.testdir
    for row in tqdm(range(len(test_df))):
        patientId , pclass = test_df.iloc[row]
        save_img_from_dcm(dcm_dir, img_dir, patientId, pclass)
    print("Test data saved as png")

    train_df["patientId"]=args.traindir+"/"+train_df['Target'].astype(str)+"/"+train_df["patientId"]+".png"
    val_df["patientId"]=args.valdir+"/"+val_df['Target'].astype(str)+"/"+val_df["patientId"]+".png"
    test_df["patientId"]=args.testdir+"/"+test_df['Target'].astype(str)+"/"+test_df["patientId"]+".png"

    train_df.to_csv("train_df.csv")
    val_df.to_csv("val_df.csv")
    test_df.to_csv("test_df.csv")

    print("Dataframes saved as csv")
    print("Done")

def main(args):
    
    """ 
    Description - Main fucntion to call preprocess(args)
    :type args: list
    :param args: list of arguments to parse
    """
    preprocess(args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('CSV', metavar="csv", type=str, help="Enter path of csv file")
    parser.add_argument('dcmdir', metavar="dcmdir", type=str, help="Enter path of file containing dicom files")
    parser.add_argument('traindir', metavar="traindir", type=str, help="Enter path of train directory")
    parser.add_argument('valdir', metavar="valdir", type=str, help="Enter path of val directory")
    parser.add_argument('testdir', metavar="testdir", type=str, help="Enter path of test directory")
    args = parser.parse_args()
    main(args)