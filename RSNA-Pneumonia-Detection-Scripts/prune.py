import argparse, os, tempfile
import train
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow_model_optimization as tfmot

def prune_model(model, train_df, train_generator, val_generator):
    
    """ 
    Description - Function to prune the model
    :type model: tensorflow model
    :param model: Model to be pruned
    
    :type train_df: DataFrame
    :param train_df: Dataframe of training data 
    
    :type train_generator: DataFrameIterator
    :param train_generator: Train data generator
    
    :type val_generator: DataFrameIterator
    :param val_generator: Validation data generator
    
    :rtype: tensorflow model
    """
    prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude

    batch_size = 32
    epochs = 2

    num_images = train_df.shape[0] 
    end_step = np.ceil(num_images / batch_size).astype(np.int32) * epochs

    # Define model for pruning.
    pruning_params = {'pruning_schedule': tfmot.sparsity.keras.PolynomialDecay(initial_sparsity=0.50,
    final_sparsity=0.80,
    begin_step=0,
    end_step=end_step)}

    model_for_pruning = prune_low_magnitude(model, **pruning_params)

    model_for_pruning.compile(optimizer='adam', 
    loss=tf.keras.losses.CategoricalCrossentropy(),
    metrics=['accuracy'])
    logdir = tempfile.mkdtemp()
    model_for_pruning.summary()
    callbacks_fn = [
        tfmot.sparsity.keras.UpdatePruningStep(),
        tfmot.sparsity.keras.PruningSummaries(log_dir=logdir)]
    
    model_for_pruning.fit(train_generator,
    batch_size=batch_size, 
    epochs=epochs, 
    validation_data = val_generator,
    callbacks=callbacks_fn)

    return model_for_pruning


def main(args):
    
    """ 
    Description - Main function
    :type args: list
    :param args: List of arguments to parse
    """
    print("Enter model type to prune - 1. Densenet, 2. Inception")
    model_type = int(input())
    if model_type == 1:
        model_name = "densenet"
    elif model_type == 2:
        model_name = "inception"
    else:
        raise Exception("Incorrect model name")
    
    model = train.build_model(model_name)
    model.load_weights(args.weightsfile)
    
    train_df = pd.read_csv("train_df.csv")
    val_df = pd.read_csv("val_df.csv")
    test_df = pd.read_csv("test_df.csv")
    train_df['Target'] = train_df['Target'].astype('str')
    val_df['Target'] = val_df['Target'].astype('str')
    test_df['Target'] = test_df['Target'].astype('str')
    train_generator, val_generator, test_generator = train.generate_batches(train_df, val_df, test_df, 32)

    pruned_model = prune_model(model,train_df, train_generator, val_generator)
    model_for_export = tfmot.sparsity.keras.strip_pruning(pruned_model)
    tf.keras.models.save_model(model_for_export, os.path.join(args.prunepath, str(model_name+"-pruned"+".h5")), include_optimizer=False)
    
    print("Pruned model saved to : ",os.path.join(args.prunepath, str(model_name+"-pruned"+".h5")))
    print("Model results on validation batch : ")
    val_loss, val_accuracy = model.evaluate(val_generator)
    print("Val loss: {:.4f}".format(val_loss))
    print("Val accuracy: {:.4f}%".format(val_accuracy * 100))

    print("Model results on test batch : ")
    test_loss, test_accuracy = model.evaluate(test_generator)
    print("Test loss: {:.4f}".format(test_loss))
    print("Test accuracy: {:.4f}%".format(test_accuracy * 100))
    print("Done")
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('weightsfile', metavar="weightsfile", type=str, help="Path to saved weights file")
    parser.add_argument('prunepath', metavar="prunepath", type=str, help="Folder to store pruned models")
    args = parser.parse_args()
    main(args)