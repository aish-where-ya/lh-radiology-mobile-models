# lh-radiology-mobile-models

Repository for low powered models for classification and detection.

https://forums.librehealth.io/t/project-low-powered-models-for-disease-detection-and-classification-for-radiology-images/3706

Link to pretrained models for RSNA Pneumonia Detection - [Click here](https://drive.google.com/drive/folders/1ZNo5jQAlJxvWN6xeaR6TBfUP4FlaZL7T?usp=sharing)

## Steps to run RSNA Pneumonia Detection scripts

1. Download the RSNA Pneumonia Detection Dataset - https://www.kaggle.com/c/rsna-pneumonia-detection-challenge/data . Clone the repo and install the requirements using -
`pip install requirements.txt`

2. Set up the directory structure and create the directories - quantized-models, trained-models and png-images.
```
├── stage_2_train_images
├── stage_2_train_labels.csv
├── RSNA-Pneumonia-Detection-Scripts
├── trained-models
├── quantized-models
├── pruned-models
├── png-images
│   ├── train
│   │   ├── 0
│   │   └── 1
│   ├── val
│   │   ├── 0
│   │   └── 1
│   ├── test
│   │   ├── 0
│   │   └──1
```
3. Preprocess the data using `data-preprocessing.py`. 
This file accepts the DICOM images, converts them to png format and saves them in train, val and test directories. This file accepts 5 positional agruments -
    1. CSV - path to csv file `stage_2_train_labels.csv`.
    2. dcmdir - path to DICOM image folder `stage_2_train_images`.
    3. traindir - path to training images `png-images/train`.
    4. valdir - path to validation images `png-images/val`.
    5. testdir - path to test images `png-images/test`.

    To run use the absolute path to each folder/file. 
    Command - `python3 data-preprocessing.py /path/to/stage_2_train_labels.csv /path/to/stage_2_train_images /path/to/png-images/train /path/to/png-images/val /path/to/png-images/test`

4. Train the model using the `train.py` script.
This file accepts the type of model to train, number to epochs to train for and saves them to a weights folder. It accepts 2 positional arguments -
    1. weightsfolder - path to folder where models and their weights will be saved `trained-models`.
    2. epochs - number of epochs to train the model.

    Use the absolute paths to all the folders.
    Command - `python3 train.py /path/to/trained-models 10`
    where 10 is the number of epochs for training.

    Upon running this script, the terminal will ask the user to enter the type of the model to train. Choose either `densenet` or `inception`. Then the model will be created, trained as well as evaluated.

5. Quantization of models using `quantize.py` scrpit.
This file accepts the model, its weights and quantizes them to the required format and stores them in the `quantized-models` directory.
It accepts 1 positional argument - 
    1. tflitepath - Path to save the quantized models `quantized-models`

    Command - `python3 quantize.py /path/to/quantized-models`.
    Upon running this, the terminal will ask the user to enter the model type to quantize. Choose either `densenet` , `inception` or `pruned`. Then, enter the path to the trained model `/path/to/model.h5`. Next, the user will be asked to enter the type of quantization -
        1. Normal tflite conversion 
        2. Dynamic quantization 
        3. Float-16 quantization
        4. Int-8 quantization
    Enter your choice 1, 2, 3 or 4 and the script will quantize your model, evaulate it and save it.

6. Pruning models using `prune.py` script - 
This file accepts the model, it's weights and prunes the model to save in the `pruned-models` directory. It accepts 2 positional arguments - 
    1. weightsfile - Path to saved weights either in .h5 or .hdf5 format `trained-models/weights.hdf5`
    2. prunepath - Path to save the pruned models `pruned-models`

    Command - `python3 prune.py /path/to/trained-models/weights.hdf5 /path/to/pruned-models`.
    Upon running this, the terminal will ask the user to enter the model type to quantize. Choose either `densenet` or `inception`. After this, the script will prune the model, evaluate it and save it.

7. Inference using `inference.py` script - 
This file accepts the image to run inference upon, and the model to run inference. It accepts 1 positional argument -
    1. imagepath - Path to PNG image to classify.

    Command - `python3 inference.py /path/to/image.png`
    The image can be of sizes - 1024x1024, 224x224, 1024x1024x3 or 224x224x3.
    Then, enter the model to run inference by picking from `densenet`, `inception`, `pruned` and `quantized` models. Accordingly, enter the `/path/to/model.h5` or `/path/to/model.tflite`. The script will return the class of the image.
